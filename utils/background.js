/**
 * Google Meeting Tool, Google Chrome Extension
 * Copyright (C) 2020  Konstantinos Chatzis <kachatzis@ece.auth.gr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



import {activateMeetingWindows} from './windowActivator.js';
import {runInterval} from './intervalRun.js';
import {cacheVars} from './cacheUtil.js';


/*
'use strict';
'script-src-elem';
'unsafe-inline';
*/



/**********************
 * Setup Listeners
 **********************/
chrome.runtime.onInstalled.addListener(function() {




  /**********************
   * Default Sync Settings
   **********************/
  chrome.storage.sync.set({autoAdmitEnabled: true});
  chrome.storage.sync.set({muteChatEnabled: false});
  chrome.storage.sync.set({muteJoinEnabled: true});
  chrome.storage.sync.set({muteParticipants: false});
  chrome.storage.sync.set({kickParticipants: false});





  /**********************
   * Timers
   **********************/

  // 1s Timer
  setInterval(function(){ runInterval(); }, 1000*1);

  // 3.5s Timer
  setInterval(function(){ fetchCache(); }, 1000*3.5);

  // 15s Timer
  setInterval(function(){ activateMeetingWindows(null); }, 1000*15);






  /***************************
   * Popup Domain Declaration
   ***************************/
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    chrome.declarativeContent.onPageChanged.addRules([{
      conditions: [new chrome.declarativeContent.PageStateMatcher({
        pageUrl: {urlContains: 'meet.google.com'},
      })
      ],
          actions: [new chrome.declarativeContent.ShowPageAction()]
    }]
    );
  });






  /****************************
   * Minimized Window Listener
   ****************************/

  chrome.windows.onFocusChanged.addListener(function(windowId) {
    activateMeetingWindows(windowId);
  });
  






 /********************************
   * Notification/Sound Listeners
   *******************************/

   // Admit Notification Listener
  chrome.webRequest.onBeforeRequest.addListener(function(details) {
    if(!cacheVars.autoAdmitEnabled)
      return null;
    return { redirectUrl: 'https://localhost/204' };
  }, {
      urls: [
        '*://www.gstatic.com/meet/sounds/knock*'
    ]
  }, ['blocking']);

  
  // Join Notifications
  chrome.webRequest.onBeforeRequest.addListener(function(details) {
    if(!cacheVars.muteJoinEnabled)
      return null;
    return { redirectUrl: 'https://localhost/204' };
  }, {
      urls: [
        '*://www.gstatic.com/meet/sounds/join*'
    ] 
  }, ['blocking']);


  // Chat Notifications
  chrome.webRequest.onBeforeRequest.addListener(function(details) {
    if(!cacheVars.muteChatEnabled)
      return null;
    return { redirectUrl: 'https://localhost/204' };
  }, {
      urls: [
        '*://www.gstatic.com/meet/sounds/group*'
    ] 
  }, ['blocking']);



});






/**********************
 * Sync Cache Builder
 **********************/
function fetchCache(){
  chrome.storage.sync.get(null, function(data){
    if(data['autoAdmitEnabled']!=cacheVars.autoAdmitEnabled)
      console.log("autoAdmitEnabled: "+data['autoAdmitEnabled']);
    cacheVars.autoAdmitEnabled = data['autoAdmitEnabled'];

    if(data['muteJoinEnabled']!=cacheVars.muteJoinEnabled)
      console.log("muteJoinEnabled: "+data['muteJoinEnabled']);
    cacheVars.muteJoinEnabled = data['muteJoinEnabled'];

    if(data['muteChatEnabled']!=cacheVars.muteChatEnabled)
      console.log("muteChatEnabled: "+data['muteChatEnabled']);
    cacheVars.muteChatEnabled = data['muteChatEnabled'];
  });
}

