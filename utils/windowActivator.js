/**
 * Google Meeting Tool, Google Chrome Extension
 * Copyright (C) 2020  Konstantinos Chatzis <kachatzis@ece.auth.gr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



 export function activateMeetingWindows(windowId) {

    
    // Only run if Auto Admit is enabled
    chrome.storage.sync.get(['autoAdmitEnabled'], data => {
        if(data.autoAdmitEnabled){

        // Uknown State, No window selected
        // Null for when we manually ask activation
        if (windowId === -1 || windowId == null) {

            // Assume already minimized or non-existant
            console.log(" > A weird window state that is: -1");

            // Fetch All windows available
            chrome.windows.getAll({populate : true}, windows => {
                windows.forEach(chromeWindow => {

                // For the minimized ones
                if (chromeWindow.state === "minimized") {

                    console.log(" > Checking minimized window...");
                    
                    // Get Tabs in the Window, and 
                    // match them against the domain
                    chrome.tabs.getAllInWindow(chromeWindow.id, windowTabs => {
                    windowTabs.forEach(tab => {
                        if(tab.url.includes('meet.google.com')){

                        console.log(" > Opening Lost Window...");

                        // Set state to normal
                        // If it was maximized before minimilization
                        // then it will sta maximized.
                        chrome.windows.update(chromeWindow.id, {state: "normal"});
                        
                        }
                    });
                    });
                }
                });
            });
            }

            // A window is caught losing focus,
            // This happens in rare cases    
            else {
            chrome.windows.get(windowId, function(chromeWindow) {
                console.log(" > Found a chaged window..."+chromeWindow.state)

                // Check for a minimized state
                if (chromeWindow.state === "minimized") {
                
                // It is minimized, Check the tabs for domain
                chrome.tabs.getAllInWindow(chromeWindow.id, windowTabs => {
                    windowTabs.forEach(tab => {
                    if(tab.url.includes('meet.google.com')){

                        console.log(" > Opening Lost Window...");

                        // Set state to normal
                        // If it was maximized before minimilization
                        // then it will sta maximized.
                        chrome.windows.update(chromeWindow.id, {state: "normal"});
                        
                    }
                    });
                });
                } 
                
                // Only Maximized and on-focus windows found
                else {
                    ;// Do noothing
                }
                });
            }
        }

    });
      
     
 }