/**
 * Google Meeting Tool, Google Chrome Extension
 * Copyright (C) 2020  Konstantinos Chatzis <kachatzis@ece.auth.gr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



import {muteParticipants} from '../utils/muteParticipants.js';
import {kickParticipants} from '../utils/kickParticipants.js';
import {autoAdmitRun} from './autoAdmit.js';



export function runInterval(){

    
    chrome.storage.sync.get(null, function(data) {


        // Auto Admit Participants
        if(data['autoAdmitEnabled']==true){
            autoAdmitRun();
        }


        // Mute Participants
        if(data['muteParticipants'] == true){
            console.log("muteParticipants: true");

            // Switch
            chrome.storage.sync.set({muteParticipants: false});

            // Mute
            muteParticipants();
        }
    
    
        // Kick Participants
        if(data['kickParticipants'] == true){
            console.log("kickParticipants: true");

            // Switch
            chrome.storage.sync.set({kickParticipants: false});

            // Kick
            kickParticipants();
        }


    });


}

