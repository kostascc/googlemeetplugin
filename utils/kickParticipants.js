/**
 * Google Meeting Tool, Google Chrome Extension
 * Copyright (C) 2020  Konstantinos Chatzis <kachatzis@ece.auth.gr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// TODO: Implement Participant Kicking
export function kickParticipants(){
    

    // Query Tabs on the Domain
    chrome.tabs.query({url: '*://meet.google.com/*'}, function(tabs) {

    console.log(" > Tabs Found: "+ tabs.length);

    // A tab exists
    // Not sure if it contains a join request yet
    if(tabs.length>0){

      // Execute Script
      chrome.tabs.executeScript(
        tabs[0].id,
        {code: 
            'alert("Not implemented yet. Contact me later to get the update!");'
            /*
            function sleep(milliseconds) {
            const date = Date.now();
            let currentDate = null;
            do {
                currentDate = Date.now();
            } while (currentDate - date < milliseconds);
            }

            // to close the sidebar (if its open) 
                    //Array.from(document.getElementsByClassName("TPpRNe")).forEach(space => { 
                        //alert("Space Check: " + space); 
                        // Continue 
                        // Sidebar should be hidden now 
                        //space.click(); 
                        // Click Profiles list button 
                        Array.from(document.getElementsByClassName("uArJ5e UQuaGc kCyAyd kW31ib foXzLb M9Bg4d")).forEach(profilesListBtn => { 
                            profilesListBtn.click(); sleep(300);
                            // Sidebar should be open at this point 
                            // Click Each Profile 
                            Array.from(document.getElementsByClassName("uArJ5e UQuaGc kCyAyd kW31ib trOFzd")).forEach(profileBar => { 
                                profileBar.click(); sleep(800);
                                // A profile button is expanded 
                                // Click Kick Button 
                                Array.from(document.getElementsByClassName("nteJvf")).forEach(kickBtn => { 
                                    kickBtn.click(); sleep(500);
                                    // A kick confirmation dialog appears 
                                    // Click Confirmation Button 
                                    Array.from(document.getElementsByClassName("U26fgb O0WRkf oG5Srb HQ8yf C0oVfc kHssdc HvOprf M9Bg4d")).forEach(confirmBtn => { 
                                        // Check Button Validity 
                                        if(confirmBtn.dataset.id=="EBS5u"){ 
                                            confirmBtn.click(); sleep(700);
                                            // A participant has been kicked 
                                            console.log("Participant Kicked"); 
                                        } 
                                    }); 
                                }); 
                            }); 
                        }); 
                    //}); 
            */
        });

        console.log("Done with the Kicking ;)");

        }

    });

}