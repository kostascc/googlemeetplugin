/**
 * Google Meeting Tool, Google Chrome Extension
 * Copyright (C) 2020  Konstantinos Chatzis <kachatzis@ece.auth.gr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



import {cacheVars} from '../utils/cacheUtil.js';



'use strict';
'unsafe-inline';




/****************************
 * Elements
 ****************************/

let autoAdmitBtn = document.getElementById('auto-admit-toggle-btn');
let muteJoinBtn = document.getElementById('mute-join-toggle-btn');
let muteChatBtn = document.getElementById('mute-chat-toggle-btn');
let muteParticipantsBtn = document.getElementById('mute-participants-btn');
let kickParticipantsBtn = document.getElementById('kick-participants-btn');






/****************************
 * Load UI
 ****************************/

onload = function onload(){
  chrome.storage.sync.get(null, function(data) {
    btnUpdate(autoAdmitBtn, data['autoAdmitEnabled']);
    btnUpdate(muteJoinBtn, data['muteJoinEnabled']);
    btnUpdate(muteChatBtn, data['muteChatEnabled']);
  });
}







/****************************
 * Event Listeners
 ****************************/

autoAdmitBtn.onclick = function(element) {
  chrome.storage.sync.get(['autoAdmitEnabled'], function(data) {
    if(data.autoAdmitEnabled == true){
      chrome.storage.sync.set({autoAdmitEnabled: false});
      btnUpdate(autoAdmitBtn, false);
      cacheVars.autoAdmitEnabled = false;
    }else{
      chrome.storage.sync.set({autoAdmitEnabled: true});
      btnUpdate(autoAdmitBtn, true);
      cacheVars.autoAdmitEnabled = true;
    }
  });
};

muteChatBtn.onclick = function(element) {
  chrome.storage.sync.get(['muteChatEnabled'], function(data) {
    if(data.muteChatEnabled == true){
      chrome.storage.sync.set({muteChatEnabled: false});
      btnUpdate(muteChatBtn, false);
      cacheVars.muteChatEnabled = false;
    }else{
      chrome.storage.sync.set({muteChatEnabled: true});
      btnUpdate(muteChatBtn, true);
      cacheVars.muteChatEnabled = true;
    }
  });
};


muteJoinBtn.onclick = function(element) {
  chrome.storage.sync.get(['muteJoinEnabled'], function(data) {
    if(data.muteJoinEnabled == true){
      chrome.storage.sync.set({muteJoinEnabled: false});
      btnUpdate(muteJoinBtn, false);
      cacheVars.muteJoinEnabled = false;
    }else{
      chrome.storage.sync.set({muteJoinEnabled: true});
      btnUpdate(muteJoinBtn, true);
      cacheVars.muteJoinEnabled = true;
    }
  });
};


muteParticipantsBtn.onclick = function(element) {
  chrome.storage.sync.set({muteParticipants: true});
};

kickParticipantsBtn.onclick = function(element) {
  chrome.storage.sync.set({kickParticipants: true});
};





/****************************
 * View Actions
 ****************************/

var btnUpdate = function autoAdmitBtnUpdate(button, state){
    if(state == true){
      button.className='waves-effect waves-light span-list-btn btn green';
    }else{
      button.className='waves-effect waves-light span-list-btn btn red';
    }
}